import cytoscape from "cytoscape";
import dagre from "cytoscape-dagre";
import elk from "cytoscape-elk";
import cola from "cytoscape-cola";
import avsdf from "cytoscape-avsdf";
import cise from "cytoscape-cise";
import coseBilkent from "cytoscape-cose-bilkent";
import fcose from "cytoscape-fcose";

const data = require("./data");
const elements = data.elements();
const typeColors = {
  1: "#66BAFF",
  2: "#559CD6",
  3: "#4C8FBF",
  4: "#5E95BC",
  5: "#719DBA",
  6: "#5A7D93",
  7: "#6D8391",
  8: "#80898E",
};

cytoscape.use(dagre);
cytoscape.use(elk);
cytoscape.use(cola);
cytoscape.use(avsdf);
cytoscape.use(cise);
cytoscape.use(coseBilkent);
cytoscape.use(fcose);

var cy = (window.cy = cytoscape({
  container: document.getElementById("cy"),

  boxSelectionEnabled: false,
  autounselectify: true,

  layout: {
    name: "cose",
    nodeDimensionsIncludeLabels: false,
    animate: false,
  },

  style: [
    {
      selector: "node",
      style: {
        "background-color": (el) => typeColors[el.attr("type")],
        // @ts-ignore
        "background-height": "40%",
        "background-width": "40%",
        "border-color": "#fff",
        "border-width": "2%",
        color: "#000",
        width: 10,
        height: 10,
        shape: "ellipse",
        title: "data(name)",
        "font-family": "Helvetica",
        "font-size": 8,
        "min-zoomed-font-size": 8,
        "overlay-opacity": 0,
      },
    },
    {
      selector: "edge",
      style: {
        "line-color": "#999",
        "overlay-opacity": 0,
        "target-arrow-color": "#999",
        "target-arrow-shape": "triangle",
        // @ts-ignore
        "target-distance-from-node": 4,
        width: 0.5,
        "source-arrow-shape": "none",
        // @ts-ignore
      },
    },
  ],
  elements,
}));
