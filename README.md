# Cytoscape.js playground

<img src="https://gitlab.com/pascal.cantaluppi/cytoscape/-/raw/main/img/cytoscape.png" />

## Network visualization with cytoscape

Social-media graph illustration

<img src="https://gitlab.com/pascal.cantaluppi/cytoscape/-/raw/main/img/graph.png" />

### Geeting started

Install dependencies

```console
npm i
```

Launch server and open browser

```console
npm start
```
